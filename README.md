# Genshin-journey frontend

## 🚧 Site currently rewriting to nuxt3

The site is currently being rewritten into [nuxt3](https://nuxt.com/) in the [nuxt3-dev](https://github.com/bot08/genshin-front/tree/nuxt3-dev) branch.  This version of main will be renamed main-legacy.

#### What's next?

- nuxt3-dev merged into nuxt3-review 
- merge nuxt3-review into main & create main-legacy branch 
- Publish release nuxt3 based version (production)

## Available Scripts

In the project directory, you can run:

#### `npm run dev`
#### `npm run build`
#### `npm run serve`

## Backend

[API docs](https://github.com/bot08/genshin-journey-API-docs)

## Some limitation 
+ Main JS bundle: <150 kib
+ Main CSS: <32 kib
+ Others: <20 kib

## v0.8.0 todo

- [ ] Add some info from categories to home page (newest banners, latest characters, random words from dictionary) (lazy components)
- [ ] Optimize big lists (partially resolved in [#26](https://github.com/bot08/genshin-front/pull/26)) (pages / virtual scroll) (characters & gacha)
- [ ] Search characters / banners / words (maybe full screen select menu)
- [ ] more content (more characters info / weapons banners)

Hybrid version v0.7.50 maybe released

## All todo
+ add weapons banners ([API example](https://sushicat.pp.ua/api/genshin/api/collections/get/gachaWeapons?token=a4191046104f8f3674f788e804c2d0))
+ vue-virtual-scroll-grid (big list optimization)
+ character more information info (talent's, constellation and resources)
+ SSR\SSG (nuxt 3 stable) (now there is only a prerender for bots)
+ fix (test with fix) site for safari 12 ([safari12-tailwind3](https://github.com/bot08/genshin-front/tree/safari12-tailwind3))
+ redesign donate card
+ weapons and items
+ design and algorithms improvement
+ mod menu in site
+ new backend ([genshin-back](https://github.com/JQweenq/genshin-back))
